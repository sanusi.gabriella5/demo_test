﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EtranBackgrdJobTSQ.Domain.Models;

namespace EtranBackgrdJobTSQ.Domain.Interface.Repositories
{
    public interface IJobRepository
    {
        Task RegisterService();
        Task LogTransactionChange(TransactionModel transaction);
    }
}

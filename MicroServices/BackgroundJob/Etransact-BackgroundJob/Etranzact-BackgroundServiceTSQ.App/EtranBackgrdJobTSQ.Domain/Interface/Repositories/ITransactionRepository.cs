﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EtranBackgrdJobTSQ.Domain.Models;

namespace EtranBackgrdJobTSQ.Domain.Interface.Repositories
{
    public interface ITransactionRepository
    {
        Task<TransactionModel[]> GetPendingBillsPayments(DateTime startDate, DateTime endDate, TransactionType transactionType);
        Task<TransactionModel[]> GetPendingInterBankTransfers(DateTime startDate, DateTime endDate, TransactionType transactionType);
        Task<TransactionModel[]> GetPendingIntraBankTransfers(DateTime startDate, DateTime endDate, TransactionType transactionType);
        Task UpdateStatus(TransactionModel transaction, TransactionStatus actualStatus);
    }

}

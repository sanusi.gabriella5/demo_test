﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EtranBackgrdJobTSQ.Domain.Models;

namespace EtranBackgrdJobTSQ.Domain.Interface.Services
{
    public interface IPaymentService
    {
        Task<PaymentStatus> GetStatus(TransactionModel transaction);
        Task Reverse(TransactionModel transaction);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EtranBackgrdJobTSQ.Domain.Models;

namespace EtranBackgrdJobTSQ.Domain.Interface.Services
{
    public interface IJobService
    {
        Task LogStartup();
        Task LogTransactionUpdate(TransactionModel transaction);
    }

}

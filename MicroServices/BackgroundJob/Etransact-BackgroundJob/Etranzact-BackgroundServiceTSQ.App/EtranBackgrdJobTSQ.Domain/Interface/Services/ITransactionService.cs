﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtranBackgrdJobTSQ.Domain.Interface.Services
{
    public interface ITransactionService
    {

        Task ProcessTransactions(DateTime date);
    }
}

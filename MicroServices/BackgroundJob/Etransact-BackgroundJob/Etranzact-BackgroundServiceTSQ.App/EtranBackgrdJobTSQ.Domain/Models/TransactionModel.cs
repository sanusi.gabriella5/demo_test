﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtranBackgrdJobTSQ.Domain.Models
{
   public class TransactionModel
    {
        public int TransactionId { get; set; }
        public string Reference { get; set; }
        public Decimal Amount { get; set; }
        public TransactionStatus Status { get; set; }
        public DateTime Date { get; set; }
        public TransactionType Type { get; set; }
        public string Narration { get; set; } //Narration plus
        public string SourceAccount { get; set; }
        public string DestinationAccount { get; set; }
        public string SourceSystem { get; set; }
        public string SourceTableName { get; set; }
        public string ResponseCode { get; set; }
        public string Stan { get; set; }

        public Request RequestBody { get; set; }
        public Response ResponseBody { get; set; }


        public class Request
        {
            public string DebitAcountNo { get; set; }
            public string CreditAccountNo { get; set; }
            public Boolean IsFees { get; set; }
            public Charge[] Charges { get; set; }
            public decimal Amount { get; set; }
            public string Reference { get; set; }
            public string Currency { get; set; }
            public string Narration { get; set; }
            public string Remark { get; set; }
            public string RequestId { get; set; }
            public string CustomerReference { get; set; }

        }

        public class Charge
        {
            public string Account { get; set; }
            public decimal Fee { get; set; }
        }

        public class Response
        {
            public ResponseDataModel Data { get; set; }
            public string Description { get; set; }
            public string Code { get; set; }


        }
        public class ResponseDataModel
        {


            public string customer_reference { get; set; }
            public decimal amount { get; set; }
            public string stan { get; set; }
        }

        /**
         * 
         * {"data":{"stan":"203197129145","customer_reference":"Remi2304211154","amount":"100"},"description":"No Action Taken","code":"21"}
         * 
         * {"DebitAccountNo":"3292573011","CreditAccountNo":"0045467018","IsFees":false,"Charges":null,"Amount":100,"Currency":"NGN",
         * "Narration":"08063242564","Remark":"CIMGAirt|wole","CustomerReference":"Remi2304211154"}
         */

    }
    public enum TransactionStatus { Successful, Pending, PendingReversal, Failed }
    public enum PaymentStatus { Successful, Failed }
    public enum TransactionType
    {
        Etranzact
    }
}

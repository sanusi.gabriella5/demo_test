﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EtranBackgrdJobTSQ.Domain.Interface.Repositories;
using EtranBackgrdJobTSQ.Domain.Interface.Services;
using EtranBackgrdJobTSQ.Domain.Models;

namespace EtranBackgrdJobTSQ.Domain.Services
{
    public class JobService : IJobService
    {
        private readonly IJobRepository _jobRepository;

        public JobService(IJobRepository jobRepository)
        {
            _jobRepository = jobRepository;
        }

        public Task LogStartup()
        {
            return _jobRepository.RegisterService();
        }

        public Task LogTransactionUpdate(TransactionModel transaction)
        {
            return _jobRepository.LogTransactionChange(transaction);
        }
    }
}

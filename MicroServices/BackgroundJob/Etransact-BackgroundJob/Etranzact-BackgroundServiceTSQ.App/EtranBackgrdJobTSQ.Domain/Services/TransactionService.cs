﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EtranBackgrdJobTSQ.Domain.Interface.Repositories;
using EtranBackgrdJobTSQ.Domain.Interface.Services;
using EtranBackgrdJobTSQ.Domain.Models;
using Microsoft.Extensions.Logging;

namespace EtranBackgrdJobTSQ.Domain.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly ILogger<TransactionService> _logger;
        private readonly IPaymentProvider _paymentProvider;
        private readonly ITransactionRepository _transaction;
        private readonly IJobService _jobService;

        public TransactionService(ILogger<TransactionService> logger, ITransactionRepository transaction, IPaymentProvider paymentProvider, IJobService jobService)
        {
            _logger = logger;
            _paymentProvider = paymentProvider;
            _transaction = transaction;
            _jobService = jobService;
        }

        public async Task ProcessTransactions(DateTime date)
        {

            var startDate = date.Date - TimeSpan.FromDays(5);
            var endDate = date.Date - TimeSpan.FromDays(1);

            var transactionTypes = Enum.GetValues(typeof(TransactionType)).Cast<TransactionType>();

            var transactions = new List<TransactionModel>();



            foreach (var transactionType in transactionTypes)
            {
                transactions.AddRange(await GetPendingTransactions(startDate, endDate, transactionType));
            }

            //Filter
            //var transactions = await _transaction.GetPendingIntraBankTransfers(startDate, endDate);
            //transactions = transactions.Where(t => t.Type == TransactionType.NIP).ToList();

            foreach (var transaction in transactions)
            {
                try
                {
                    await ProcessTransaction(transaction);
                }
                catch (Exception ex)
                {
                    //Log Exception for now
                    _logger.LogError(ex, $"An Error Occurred processing Transction: {transaction.Reference}");
                }
            }
        }

        private async Task<TransactionModel[]> GetPendingTransactions(DateTime startDate, DateTime endDate, TransactionType transactionType)
        {

            var pendingBills = await _transaction.GetPendingBillsPayments(startDate, endDate, transactionType);
            var pendingInterBankTransfers = await _transaction.GetPendingInterBankTransfers(startDate, endDate, transactionType);
            var pendingIntraBankTransfers = await _transaction.GetPendingIntraBankTransfers(startDate, endDate, transactionType);


            var transactions = pendingBills.Union(pendingInterBankTransfers).Union(pendingIntraBankTransfers).ToArray();

            _logger.LogInformation("Fetched {pendingBillsCount} Pending BillsPayments", pendingBills.Length);
            _logger.LogInformation("Fetched {pendingInterBankTransfersCount} Pending InterBankTransfersPayments", pendingInterBankTransfers.Length);
            _logger.LogInformation("Fetched {pendingIntraBankTransfersCount} Pending IntraBankTransfersPayments", pendingIntraBankTransfers.Length);

            _logger.LogInformation("Returned Total of {totalPendingTransactions} transactions", transactions.Length);
            return transactions;
        }

        public async Task ProcessTransaction(TransactionModel transaction)
        {
            _logger.LogInformation($"Processing Transaction: {transaction.Reference}");

            if (transaction.Status != TransactionStatus.Pending) throw new Exception("Transaction is not Pending");

            var paymentService = _paymentProvider.GetPaymentService(transaction.Type);

            var actualStatus = await paymentService.GetStatus(transaction);   //break point here

            switch (actualStatus)
            {
                case PaymentStatus.Failed:
                    await _transaction.UpdateStatus(transaction, TransactionStatus.PendingReversal);
                    await paymentService.Reverse(transaction);
                    await _jobService.LogTransactionUpdate(transaction);
                    break;
                case PaymentStatus.Successful:
                    await _transaction.UpdateStatus(transaction, TransactionStatus.Successful);
                    break;
            }
        }

    }
}

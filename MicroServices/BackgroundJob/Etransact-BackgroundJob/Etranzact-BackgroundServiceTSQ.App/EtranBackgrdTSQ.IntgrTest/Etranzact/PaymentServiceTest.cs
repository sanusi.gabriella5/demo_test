﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using EtranBackgrdJobTSQ.Domain.Models;
using EtranBackgrdJobTSQ.Infras.Services.Etranzact;
using Moq;
using Xunit;
using static EtranBackgrdJobTSQ.Infras.Services.PaymentProvider;

namespace EtranBackgrdTSQ.IntgrTest.Etranzact
{
    public class PaymentServiceTest
    {
        [Theory]
        //[InlineData("ETRZTB2C0000000033", PaymentStatus.Successful)]
        // [InlineData("ETRZTB2C0000000034", PaymentStatus.Successful)]
        [InlineData("ETRZTB2C0000000039", PaymentStatus.Failed)]

        public async Task GetTransactionStatus(string reference, PaymentStatus expectedStatus)
        {
            //Arrange
            var options = new Options
            {
                Etranzact = new EtranzactOptions
                {
                    EtranzactUrl = new Uri("https://etransactmcsvc.fcmb-azr-msase.p.azurewebsites.net"),
                    CorrelationId = "Ef0799f38-8860-4fcb-8120-876b2d325d7a",
                    ClientId = "Test_123"
                }
            };
            var transaction = new TransactionModel { Reference = reference };

            var mock = new Mock<IHttpClientFactory>();

            mock.Setup(f => f.CreateClient(It.IsAny<string>())).Returns(new HttpClient());

            var sut = new PaymentService(options, mock.Object);

            //Act
            var actualStatus = await sut.GetStatus(transaction);

            //Assert
            Assert.Equal(expectedStatus, actualStatus);
        }
        [Theory]
        [InlineData("ETRZTB2C0000000039")]
        public async Task ReverseTransaction(string reference)
        {
            //Arrange   
            var options = new Options
            {
                Etranzact = new EtranzactOptions
                {
                    ReversalUrl = new Uri("https://intrabankftmcsvc.fcmb-azr-msase.p.azurewebsites.net"),
                    CorrelationId = "f0799f38-8860-4fcb-8120-876b2d325d7a",
                    ClientId = "Test_123"
                }
            };
            var mock = new Mock<IHttpClientFactory>();
            mock.Setup(f => f.CreateClient(It.IsAny<string>())).Returns(new HttpClient());
            var sut = new PaymentService(options, mock.Object);
            var transaction = new TransactionModel
            {
                Reference = reference,
                Amount = 10,
                Narration = "08045632354",
                SourceSystem = "System",
                Date = DateTime.Now,
                SourceAccount = "0045467018",
                SourceTableName = "ETZ_FundTransfer",
                DestinationAccount = "3292573011",
                Stan = "883750791937",
                ResponseCode = "99",
                RequestBody = new TransactionModel.Request
                {
                    //Reference = "ETRZTB2C0000000039",
                    CreditAccountNo = "0045467018",
                    IsFees = false,
                    Charges = null,
                    Currency = "NGN",
                    Narration = "Chijioke Dev Testing",
                    DebitAcountNo = "3292573011",
                    Remark = "ROSS ENIAYO KUTEYI",
                    CustomerReference = "Remi2304211154"

                },
                ResponseBody = new TransactionModel.Response
                {
                    Code = "21",
                    Data = new TransactionModel.ResponseDataModel
                    {
                        stan = "883750791937",
                        customer_reference = "Remi2304211154",
                        amount = 10,
                    },
                    Description = "No Action Taken"
                }
            };
            //Act   
            await sut.Reverse(transaction);  //break point here 
        }
    }

}

﻿using System;
using Infra = EtranBackgrdTSQ.Infrastructure.Services;

namespace EtranBackgrdTSQ.UintTest.Infrastructure
{
    public class PaymentProviderTests
    {
        [Theory]
        [InlineData(TransactionType.Etranzact, typeof(Infra.Etranzact.PaymentService))]
        public void GetPaymentServiceTest(TransactionType transactionType, Type serviceType)
        {
            //Arrange
            var provider = new PaymentProvider(null, null);

            //Act
            var service = provider.GetPaymentService(transactionType);


            //Assert
            Assert.IsType(serviceType, service);
        }
    }
    }

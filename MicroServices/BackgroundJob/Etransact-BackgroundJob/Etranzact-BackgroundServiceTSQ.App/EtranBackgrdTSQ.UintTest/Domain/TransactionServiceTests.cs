﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtranBackgrdTSQ.UintTest.Domain
{
    public class TransactionServiceTests
    {
        [Fact]
        public async Task ProcessTransactionSelectsDayOldTransactions()
        {
            //Arrange
            var logger = new Mock<ILogger<TransactionService>>();
            var transactionRepo = new Mock<ITransactionRepository>();
            var payment = new Mock<IPaymentProvider>();
            var jobService = new Mock<IJobService>();

            var sut = new TransactionService(logger.Object, transactionRepo.Object, payment.Object, jobService.Object);
            var execDate = DateTime.Now;
            var cutOffDate = DateTime.Now - TimeSpan.FromDays(1);

            //Act
            await sut.ProcessTransactions(execDate);

            //Assert
            //  transactionRepo.Verify(t => t.GetPendingBillsPayments(It.IsAny<DateTime>(), It.Is<DateTime>(d => d < cutOffDate)), "Transaction Date Fetched was within 24hrs");
            //  transactionRepo.Verify(t => t.GetPendingInterBankTransfers(It.IsAny<DateTime>(), It.Is<DateTime>(d => d < cutOffDate)), "Transaction Date Fetched was within 24hrs");
            // transactionRepo.Verify(t => t.GetPendingIntraBankTransfers(It.IsAny<DateTime>(), It.Is<DateTime>(d => d < cutOffDate)), "Transaction Date Fetched was within 24hrs");



        }
        [Fact]
        public async Task SuccessfulTransactionsShouldUpdateDatabase()
        {
            //Arrange
            var logger = new Mock<ILogger<TransactionService>>();
            var transactionRepo = new Mock<ITransactionRepository>();
            var payment = new Mock<IPaymentProvider>();
            var payService = new Mock<IPaymentService>();
            var jobService = new Mock<IJobService>();

            var transaction = new TransactionModel
            {
                Status = TransactionStatus.Pending,
                Reference = "ABC123",
                Type = TransactionType.NIP
            };

            payment.Setup(p => p.GetPaymentService(TransactionType.NIP)).Returns(payService.Object);

            payService.Setup(p => p.GetStatus(transaction)).Returns(Task.FromResult(PaymentStatus.Successful));

            var sut = new TransactionService(logger.Object, transactionRepo.Object, payment.Object, jobService.Object);

            //Act
            await sut.ProcessTransaction(transaction);

            //Assert
            transactionRepo.Verify(t => t.UpdateStatus(transaction, TransactionStatus.Successful));
            payService.Verify(p => p.Reverse(transaction), Times.Never);
        }


        [Fact]
        public async Task FailedTransactionsShouldTriggerReversal()
        {
            //Arrange
            var logger = new Mock<ILogger<TransactionService>>();
            var transactionRepo = new Mock<ITransactionRepository>();
            var payment = new Mock<IPaymentProvider>();
            var payService = new Mock<IPaymentService>();
            var jobService = new Mock<IJobService>();

            var TransactionModel = new TransactionModel
            {
                Status = TransactionStatus.Pending,
                Reference = "ABC123",
                Type = TransactionType.NIP
            };

            payment.Setup(p => p.GetPaymentService(TransactionType.NIP)).Returns(payService.Object);

            payService.Setup(p => p.GetStatus(TransactionModel)).Returns(Task.FromResult(PaymentStatus.Failed));

            var sut = new TransactionService(logger.Object, transactionRepo.Object, payment.Object, jobService.Object);

            //Act
            await sut.ProcessTransaction(TransactionModel);

            //Assert
            //transactionRepo.Verify(t => t.UpdateStatus(transaction, TransactionStatus.Failed));
            transactionRepo.Verify(t => t.UpdateStatus(TransactionModel, TransactionStatus.PendingReversal));
            payService.Verify(p => p.Reverse(TransactionModel));
        }

        [Fact]
        public async Task PendingTransactionsShouldDoNothing()
        {
            //Arrange
            var logger = new Mock<ILogger<TransactionService>>();
            var transactionRepo = new Mock<ITransactionRepository>();
            var payment = new Mock<IPaymentProvider>();
            var payService = new Mock<IPaymentService>();
            var jobService = new Mock<IJobService>();

            var transaction = new TransactionModel
            {
                Status = TransactionStatus.Pending,
                Reference = "ABC123",
                Type = TransactionType.NIP
            };

            payment.Setup(p => p.GetPaymentService(TransactionType.NIP)).Returns(payService.Object);

            payService.Setup(p => p.GetStatus(transaction)).Returns(Task.FromResult(PaymentStatus.Successful));

            var sut = new TransactionService(logger.Object, transactionRepo.Object, payment.Object, jobService.Object);

            //Act
            await sut.ProcessTransaction(transaction);

            //Assert
            //transactionRepo.Verify(t => t.UpdateStatus(It.IsAny<TransactionModel>(), It.IsAny<TransactionStatus>()), Times.Never);
            transactionRepo.Verify(t => t.UpdateStatus(It.IsAny<TransactionModel>(), It.IsAny<TransactionStatus>()));
            payService.Verify(p => p.Reverse(It.IsAny<TransactionModel>()), Times.Never);
        }
    }

}

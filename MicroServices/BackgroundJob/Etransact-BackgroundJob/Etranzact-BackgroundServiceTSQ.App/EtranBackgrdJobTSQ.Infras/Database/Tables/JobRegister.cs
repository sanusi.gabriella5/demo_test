﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtranBackgrdJobTSQ.Infras.Database.Tables
{
    [Table("tbl_BackgroundJob_Register")]
    public class JobRegister
    {
        [Key]
        public int ID { get; set; }
        public string AppName { get; set; }
        public string LastTransPick { get; set; }
        public DateTime RecordDate { get; set; }
        public string Status { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtranBackgrdJobTSQ.Infras.Database.Tables
{
    [Table("tbl_orchestration_billspayment")]
    public class BillsPayment
    {
        [Key]
        public int ID { get; set; }
        public string Status { get; set; }
        public decimal? Amount { get; set; }
        public string Reference { get; set; }
        public string Action { get; set; }
        public DateTime? ResponseTime { get; set; }

        public string SourceAccount { get; set; }
        public string DestinationAccount { get; set; }
        public string ResponseCode { get; set; }
        public string RequestBody { get; set; }
        public string ResponseBody { get; set; }
    }
    }

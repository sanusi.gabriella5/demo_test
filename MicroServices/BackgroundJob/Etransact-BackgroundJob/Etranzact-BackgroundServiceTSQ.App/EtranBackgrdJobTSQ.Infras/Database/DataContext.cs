﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EtranBackgrdJobTSQ.Infras.Database.Tables;
using Microsoft.EntityFrameworkCore;

namespace EtranBackgrdJobTSQ.Infras.Database
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<BillsPayment> BillsPayments { get; set; }
        public DbSet<IntrabankTransfer> IntrabankTransfers { get; set; }
        public DbSet<InterbankTransfer> InterbankTransfers { get; set; }
        public DbSet<JobRegister> JobRegisters { get; set; }
        public DbSet<TransactionLog> TransactionLogs { get; set; }
    }

}

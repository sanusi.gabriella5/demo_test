﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace EtranBackgrdJobTSQ.Infras
{
    public static class Extensions
    {
        public static Uri AddQueryParams(this Uri url, Dictionary<string, string> values)
        {
            NameValueCollection queryString = System.Web.HttpUtility.ParseQueryString("");

            foreach (KeyValuePair<string, string> pair in values)
            {
                queryString.Add(pair.Key, pair.Value);
            }
            var fullUrl = new Uri($"{url}?{queryString}");
            return fullUrl;
        }

        public static Uri AddQueryParam(this Uri url, string param, string value)
        {
            NameValueCollection queryString = System.Web.HttpUtility.ParseQueryString("");

            queryString.Add(param, value);

            var fullUrl = new Uri($"{url}?{queryString}");
            return fullUrl;
        }
    }
}

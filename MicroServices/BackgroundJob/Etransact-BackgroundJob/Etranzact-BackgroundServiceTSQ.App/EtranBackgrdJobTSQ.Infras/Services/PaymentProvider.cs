﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using EtranBackgrdJobTSQ.Domain.Interface.Services;
using EtranBackgrdJobTSQ.Domain.Models;

namespace EtranBackgrdJobTSQ.Infras.Services
{
    public class PaymentProvider : IPaymentProvider
    {
        private readonly Options _options;
        private readonly IHttpClientFactory _httpClientFactory;

        public PaymentProvider(Options options, IHttpClientFactory httpClientFactory)
        {
            _options = options;
            _httpClientFactory = httpClientFactory;
        }
        public IPaymentService GetPaymentService(TransactionType transactionType)
        {
            switch (transactionType)
            {
                case TransactionType.Etranzact: //To complete the other Etranzact-biz
                    return new Etranzact.PaymentService(_options, _httpClientFactory);
               
                default:
                    throw new Exception("Unknown Payment Provider");
            }
        }
        public class Options
        {
            public EtranzactOptions Etranzact { get; set; } = new EtranzactOptions();

        }

        public class EtranzactOptions
        {
            public Uri EtranzactUrl { get; set; }
            public Uri ReversalUrl { get; set; }
            public string CorrelationId { get; set; }
            public string ClientId { get; set; }

        }

    }

}

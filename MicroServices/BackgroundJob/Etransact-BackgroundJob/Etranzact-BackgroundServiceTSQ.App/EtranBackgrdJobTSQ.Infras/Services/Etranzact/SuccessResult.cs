﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtranBackgrdJobTSQ.Infras.Services.Etranzact
{
    public class SuccessResult
    {
        public SuccessResult()
        {
        }

        public RespResult Resp { get; set; }

        public class RespResult
        {
            public string Code { get; set; }
            public string Status { get; set; }
            public string Description { get; set; }
        }
    }
}

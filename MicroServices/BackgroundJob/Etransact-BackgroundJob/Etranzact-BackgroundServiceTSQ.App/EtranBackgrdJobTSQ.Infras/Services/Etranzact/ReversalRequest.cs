﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtranBackgrdJobTSQ.Infras.Services.Etranzact
{
    public class ReversalRequest
    {
        public int RowId { get; set; }
        public string OriginalRequestId { get; set; }
        public DateTime TransactionDate { get; set; }
        public string RequestStan { get; set; }
        public string SourceAccount { get; set; }
        public string DestinationAccount { get; set; }
        public string SourceSystem { get; set; }
        public string SourceTableName { get; set; }
        public decimal Amount { get; set; }
        public decimal TotalFee { get; set; }
        public string ResponseCode { get; set; }
        public string ClientId { get; set; }
        public string Narration { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using EtranBackgrdJobTSQ.Domain.Interface.Services;
using EtranBackgrdJobTSQ.Domain.Models;
using Newtonsoft.Json;

namespace EtranBackgrdJobTSQ.Infras.Services.Etranzact
{
    public class PaymentService : IPaymentService
    {
        private readonly PaymentProvider.Options _options;
        private readonly IHttpClientFactory _httpClientFactory;

        public PaymentService(PaymentProvider.Options options, IHttpClientFactory httpClientFactory)
        {
            _options = options;
            _httpClientFactory = httpClientFactory;
        }

        public async Task<PaymentStatus> GetStatus(TransactionModel transaction)
        {
            var client = _httpClientFactory.CreateClient();

            Uri fullUri = new Uri(_options.Etranzact.EtranzactUrl, "/api/Etranzact/transactionStatus");

            client.DefaultRequestHeaders.Add("client_id", _options.Etranzact.ClientId);
            client.DefaultRequestHeaders.Add("x-correlation-id", Guid.NewGuid().ToString());

            var payload = new
            {
                Reference = transaction.Reference
            };
            var requestString = JsonConvert.SerializeObject(payload);
            var content = new StringContent(requestString, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(fullUri, content);
            var responseString = await response.Content.ReadAsStringAsync();
            switch (response.StatusCode)
            {
                case System.Net.HttpStatusCode.OK:
                    var successResult = JsonConvert.DeserializeObject<SuccessResult>(responseString);
                    if (successResult.Resp == null) throw new Exception("Payment Provider did not return the Transaction");
                    var status = successResult.Resp.Status;//Pls confirm here
                    return GetPaymentStatus(status);
                case System.Net.HttpStatusCode.BadRequest:
                    return PaymentStatus.Failed;
                    throw new Exception($"Provider Returned Bad Request");
                case System.Net.HttpStatusCode.InternalServerError:
                    var errorResult = JsonConvert.DeserializeObject<ServerErrorResult>(responseString);

                    if (errorResult.Type == "99")
                    {
                        return PaymentStatus.Failed;
                    }
                    throw new Exception("Payment Provider Returned Internal Server.");

                case System.Net.HttpStatusCode.NotFound:
                    return PaymentStatus.Failed;
                //throw new Exception("Payment Provider could not find the Transaction.");
                default:
                    throw new Exception($"Invalid Status Code Received: {response.StatusCode}");
            }

        }

        public async Task Reverse(TransactionModel transaction)
        {
            var client = _httpClientFactory.CreateClient();

            Uri baseUrl = new Uri($"{_options.Etranzact.ReversalUrl}/api/IntrabankTransfer/logReversal");
            Uri requestUrl = baseUrl.AddQueryParam("invoiceno", "xyz123");

            var reversalRequest = new ReversalRequest
            {
                ClientId = _options.Etranzact.ClientId,
                Amount = transaction.Amount,
                TransactionDate = transaction.Date,
                Narration = transaction.RequestBody?.Narration,
                RowId = transaction.TransactionId,
                RequestStan = transaction.Stan,
                OriginalRequestId = transaction.TransactionId.ToString(),
                SourceAccount = transaction.SourceAccount,
                DestinationAccount = transaction.DestinationAccount,
                SourceSystem = transaction.SourceSystem,
                SourceTableName = transaction.SourceTableName,
                TotalFee = transaction.Amount,
                ResponseCode = transaction.ResponseBody.Code

            };
            var requestString = JsonConvert.SerializeObject(reversalRequest);
            var content = new StringContent(requestString, Encoding.UTF8, "application/json");

            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = requestUrl,
                Content = content

            };

            request.Headers.Add("accept", "application/json");
            request.Headers.Add("client_id", _options.Etranzact.ClientId);
            request.Headers.Add("correlationId", _options.Etranzact.CorrelationId);

            var response = await client.SendAsync(request);

            var responseString = await response.Content.ReadAsStringAsync();
            switch (response.StatusCode)  //breakpoint here
            {
                case System.Net.HttpStatusCode.OK:
                    //var successResult = JsonConvert.DeserializeObject<SuccessResult.RespResult>(responseString);
                    var successResult = JsonConvert.DeserializeObject<SuccessResult>(responseString);
                    //var status = successResult.Status;
                    var status = successResult.Resp;//checked here
                    break;
                case System.Net.HttpStatusCode.BadRequest:
                    var result = JsonConvert.DeserializeObject<BadRequestResult>(responseString);
                    throw new Exception($"Payment Provider Returned Bad Request.: {result}");
                case System.Net.HttpStatusCode.InternalServerError:
                    //var errorResult = JsonConvert.DeserializeObject<ServerErrorResult>(responseString);
                    throw new Exception("Payment Provider Returned Internal Server.");
                default:
                    throw new Exception($"Invalid Status Code Received: {response.StatusCode}");
            }
        }

        public PaymentStatus GetPaymentStatus(string status)
        {
            switch (status.ToLower())
            {
                case "00":
                    return PaymentStatus.Successful;
                default:
                    return PaymentStatus.Failed;
            }
        }

    }




}

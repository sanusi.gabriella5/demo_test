﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtranBackgrdJobTSQ.Infras.Services.Etranzact
{
    public class BadRequestResult
    {
        public BadRequestResult()
        {
        }

        public string Type { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }
        public string Detail { get; set; }
    }


}

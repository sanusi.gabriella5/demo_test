﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EtranBackgrdJobTSQ.Domain.Interface.Repositories;
using EtranBackgrdJobTSQ.Domain.Models;
using EtranBackgrdJobTSQ.Infras.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace EtranBackgrdJobTSQ.Infras.Repositories
{
    public class TransactionRepository : ITransactionRepository
    {
        private readonly ILogger<TransactionRepository> _logger;
        private readonly DataContext _context;

        public TransactionRepository(DataContext context, ILogger<TransactionRepository> logger)
        {
            _logger = logger;
            _context = context;
        }
        public async Task<TransactionModel[]> GetPendingBillsPayments(DateTime startDate, DateTime endDate, TransactionType transactionType)//replicate
        {
            var action = GetPaymentAction(transactionType);
            var query = from billsPayments in _context.BillsPayments
                        where billsPayments.Action == action
                        where billsPayments.Status == "P"
                        select billsPayments;

            var records = await query.ToListAsync();

            var transactions = new List<TransactionModel>();
            foreach (var record in records)
            {
                try
                {
                    var paymentType = GetPaymentType(record.Action);
                    TransactionStatus transactionStatus = GetStatus(record.Status);
                    TransactionModel.Request requestBody = JsonConvert.DeserializeObject<TransactionModel.Request>(record.RequestBody ?? "");
                    TransactionModel.Response responseBody = JsonConvert.DeserializeObject<TransactionModel.Response>(record.ResponseBody ?? "");
                    var transaction = new TransactionModel
                    {
                        Reference = record.Reference,
                        Status = transactionStatus,
                        Amount = record.Amount ?? 0,
                        Date = record.ResponseTime ?? new DateTime(),
                        SourceTableName = record.Action,
                        DestinationAccount = record.DestinationAccount,
                        SourceAccount = record.SourceAccount,
                        ResponseCode = record.ResponseCode,
                        SourceSystem = paymentType.ToString(),
                        RequestBody = requestBody,
                        ResponseBody = responseBody,
                        TransactionId = record.ID,
                        Type = GetPaymentType(record.Action)
                    };
                    transactions.Add(transaction);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Could not Ingest Transaction {reference}", record.Reference);
                }
            }
            return transactions.ToArray();
        }

        public async Task<TransactionModel[]> GetPendingIntraBankTransfers(DateTime startDate, DateTime endDate, TransactionType transactionType)
        {
            var action = GetPaymentAction(transactionType);
            var query = from intraBankTransfers in _context.IntrabankTransfers
                        where intraBankTransfers.Action == action
                        where intraBankTransfers.Status == "P"
                        select intraBankTransfers;

            var records = await query.ToListAsync();

            var transactions = new List<TransactionModel>();
            foreach (var record in records)
            {
                try
                {
                    var paymentType = GetPaymentType(record.Action);
                    TransactionStatus transactionStatus = GetStatus(record.Status);
                    TransactionModel.Request requestBody = JsonConvert.DeserializeObject<TransactionModel.Request>(record.RequestBody ?? "");
                    TransactionModel.Response responseBody = JsonConvert.DeserializeObject<TransactionModel.Response>(record.ResponseBody ?? "");
                    var transaction = new TransactionModel
                    {
                        Reference = record.Reference,
                        Status = transactionStatus,
                        Amount = record.Amount ?? 0,
                        Date = record.ResponseTime ?? new DateTime(),
                        SourceTableName = record.Action,
                        DestinationAccount = record.DestinationAccount,
                        SourceAccount = record.SourceAccount,
                        ResponseCode = record.ResponseCode,
                        SourceSystem = paymentType.ToString(),
                        RequestBody = requestBody,
                        ResponseBody = responseBody,
                        TransactionId = record.ID,
                        Type = GetPaymentType(record.Action)
                    };
                    transactions.Add(transaction);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Could not Ingest Transaction {reference}", record.Reference);
                }
            }
            return transactions.ToArray();
        }

        public async Task<TransactionModel[]> GetPendingInterBankTransfers(DateTime startDate, DateTime endDate, TransactionType transactionType)
        {
            var action = GetPaymentAction(transactionType);
            var query = from interBankTransfers in _context.InterbankTransfers
                        where interBankTransfers.Action == action
                        where interBankTransfers.Status == "P"
                        select interBankTransfers;

            var records = await query.ToListAsync();

            var transactions = new List<TransactionModel>();
            foreach (var record in records)
            {
                try
                {
                    var paymentType = GetPaymentType(record.Action);
                    TransactionModel.Request requestBody = JsonConvert.DeserializeObject<TransactionModel.Request>(record.RequestBody ?? "");
                    TransactionModel.Response responseBody = JsonConvert.DeserializeObject<TransactionModel.Response>(record.ResponseBody ?? "");
                    TransactionStatus transactionStatus = GetStatus(record.Status);
                    var transaction = new TransactionModel
                    {
                        Reference = record.Reference,
                        Status = transactionStatus,
                        Amount = record.Amount ?? 0,
                        Date = record.ResponseTime ?? new DateTime(),
                        SourceTableName = record.Action,
                        DestinationAccount = record.DestinationAccount,
                        SourceAccount = record.SourceAccount,
                        ResponseCode = record.ResponseCode,
                        RequestBody = requestBody,
                        ResponseBody = responseBody,
                        TransactionId = record.ID,
                        Type = transactionType,
                        Stan = record.Stan
                    };
                    transactions.Add(transaction);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Could not Ingest Transaction {reference}", record.Reference);
                }
            }
            return transactions.ToArray();
        }

        private TransactionStatus GetStatus(string status)
        {
            return (status.ToLower()) switch
            {
                "s" => TransactionStatus.Successful,
                "f" => TransactionStatus.Failed,
                "p" => TransactionStatus.Pending,
                "l" => TransactionStatus.PendingReversal,
                _ => TransactionStatus.Pending,
            };
        }

        private TransactionType GetPaymentType(string paymentType)
        {
            var paymentTypeLower = paymentType?.ToLower().Trim();
            switch (paymentTypeLower)
            {
                case "etz_fundtransfer":
                    return TransactionType.Etranzact;
                //Add the other Etran-biz case

                default:
                    throw new Exception($"Unknown Payment Type: {paymentType}");
            }
        }

        private string GetPaymentAction(TransactionType transactionType)
        {
            switch (transactionType)
            {
                
                case TransactionType.Etranzact:
                    return "etz_fundtransfer";
                //Add the other Etran-biz case

                default:
                    return transactionType.ToString();
            }
        }

        private string ToStatusString(TransactionStatus status)
        {
            switch (status)
            {
                case TransactionStatus.Pending:
                    return "P";
                case TransactionStatus.PendingReversal:
                    return "L";
                case TransactionStatus.Successful:
                    return "S";
                case TransactionStatus.Failed:
                    return "F";
                default:
                    throw new Exception("Status cannot be converted to Status Letter");
            }
        }

        public async Task UpdateStatus(TransactionModel transaction, TransactionStatus actualStatus)
        {
            var billPayment = await _context.BillsPayments.Where(b => b.Reference == transaction.Reference).FirstOrDefaultAsync();
            if (billPayment != null)
            {
                billPayment.Status = ToStatusString(actualStatus);
                _context.Update(billPayment);
                _context.SaveChanges();
                return;
            }

            var interBankTransfer = await _context.InterbankTransfers.Where(i => i.Reference == transaction.Reference).FirstOrDefaultAsync();
            if (interBankTransfer != null)
            {
                interBankTransfer.Status = ToStatusString(actualStatus);
                _context.Update(interBankTransfer);
                _context.SaveChanges();
                return;
            }


            var intraBankTransfer = await _context.IntrabankTransfers.Where(i => i.Reference == transaction.Reference).FirstOrDefaultAsync();
            if (intraBankTransfer != null)
            {
                intraBankTransfer.Status = ToStatusString(actualStatus);
                _context.Update(intraBankTransfer);
                _context.SaveChanges();
                return;
            }
        }

    }


}

    

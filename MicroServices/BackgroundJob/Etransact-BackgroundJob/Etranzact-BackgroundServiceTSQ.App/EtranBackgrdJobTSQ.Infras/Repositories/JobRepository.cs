﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EtranBackgrdJobTSQ.Domain.Interface.Repositories;
using EtranBackgrdJobTSQ.Domain.Models;
using EtranBackgrdJobTSQ.Infras.Database;
using EtranBackgrdJobTSQ.Infras.Database.Tables;

namespace EtranBackgrdJobTSQ.Infras.Repositories
{
    public class JobRepository : IJobRepository
    {
        private readonly DataContext _context;

        public JobRepository(DataContext context)
        {
            _context = context;
        }

        public async Task LogTransactionChange(TransactionModel transaction)
        {
            var registerJobLog = new TransactionLog
            {

                RequestId = "",
                Action = "",
                Reference = transaction.Reference,
                CorrelationId = "",
                SourceAccount = "",

                DestinationAccount = "",
                Amount = transaction.Amount,
                RequestBody = "",
                ResponseBody = "",
                RequestTime = DateTime.Now,
                ResponseTime = DateTime.Now,


                ResponseCode = "",
                ResponseMessage = "",
                ClientId = "",
                Remark = "",
                ApiUrl = ""
            };

            _context.TransactionLogs.Add(registerJobLog);

            await _context.SaveChangesAsync();
        }

        public async Task RegisterService()
        {
            var register = new JobRegister
            {
                AppName = "",
                LastTransPick = "",
                RecordDate = DateTime.Now,
                Status = ""
            };

            _context.JobRegisters.Add(register);

            await _context.SaveChangesAsync();
        }
    }



    }

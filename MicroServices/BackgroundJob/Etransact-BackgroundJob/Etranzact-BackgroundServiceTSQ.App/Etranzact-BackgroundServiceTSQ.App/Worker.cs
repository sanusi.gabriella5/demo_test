using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Etranzact_BackgroundServiceTSQ.App
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;

        private readonly ITransactionService _transactionService;

        private readonly IJobService _jobService;

        public Worker(ILogger<Worker> logger, ITransactionService transactionService, IJobService jobService)
        {
            _logger = logger;
            _transactionService = transactionService;

            _jobService = jobService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await _jobService.LogStartup();
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                await _transactionService.ProcessTransactions(DateTime.Now);
                await Task.Delay(1000, stoppingToken);
            }
        }
    }
}

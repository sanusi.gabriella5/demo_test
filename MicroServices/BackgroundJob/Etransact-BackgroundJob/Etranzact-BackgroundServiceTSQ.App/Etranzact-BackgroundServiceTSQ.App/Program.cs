using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Etranzact_BackgroundServiceTSQ.App.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace Etranzact_BackgroundServiceTSQ.App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddAppServices(hostContext.Configuration);
                    services.AddHostedService<Worker>();
                })
                .UseSerilog((hostingContext, services, loggerConfiguration) =>
                    loggerConfiguration
                        .ReadFrom.Configuration(hostingContext.Configuration)
                        .Enrich.FromLogContext()
                        .WriteTo.Console());

    }
}

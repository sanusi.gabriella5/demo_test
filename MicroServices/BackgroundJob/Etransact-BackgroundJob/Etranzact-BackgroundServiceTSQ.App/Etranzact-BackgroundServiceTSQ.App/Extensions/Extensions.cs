﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EtranBackgrdJobTSQ.Domain.Interface.Repositories;
using EtranBackgrdJobTSQ.Domain.Interface.Services;
using EtranBackgrdJobTSQ.Domain.Services;
using EtranBackgrdJobTSQ.Infras.Database;
using EtranBackgrdJobTSQ.Infras.Repositories;
using EtranBackgrdJobTSQ.Infras.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Etranzact_BackgroundServiceTSQ.App.Extensions
{
    public static class Extensions
    {
        public static void AddAppServices(this IServiceCollection services, IConfiguration configuration)
        {
            var providerOptions = configuration.GetSection("PaymentProvider").Get<PaymentProvider.Options>();

            services.AddSingleton<ITransactionService, TransactionService>();
            services.AddSingleton<ITransactionRepository, TransactionRepository>();
            services.AddSingleton<IJobService, JobService>();
            services.AddSingleton<IJobRepository, JobRepository>();

            services.AddSingleton<IPaymentProvider, PaymentProvider>();
            services.AddSingleton(sp => providerOptions);

            services.AddHttpClient();

            var connectionString = configuration.GetConnectionString("DataContext");

            services.AddDbContext<DataContext>(options => options.UseSqlServer(connectionString), ServiceLifetime.Singleton);


        }
    }
}
